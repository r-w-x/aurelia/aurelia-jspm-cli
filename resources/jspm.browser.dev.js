SystemJS.config({
  baseURL: ".",
  mockApi: false,
  paths: {
    "github:": "jspm_packages/github/",
    "npm:": "jspm_packages/npm/",
    "app/": "src/"
  }
});
