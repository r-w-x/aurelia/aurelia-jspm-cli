SystemJS.config({
  nodeConfig: {
    "paths": {
      "github:": "jspm_packages/github/",
      "npm:": "jspm_packages/npm/",
      "app/": "src/"
    }
  },
  devConfig: {
    "map": {
      "plugin-typescript": "github:frankwallis/plugin-typescript@9.0.0",
      "typescript": "npm:typescript@2.9.2",
      "os": "npm:jspm-nodelibs-os@0.2.2",
      "net": "npm:jspm-nodelibs-net@0.2.1",
      "crypto": "npm:jspm-nodelibs-crypto@0.2.1",
      "vm": "npm:jspm-nodelibs-vm@0.2.1",
      "util": "npm:jspm-nodelibs-util@0.2.2",
      "stream": "npm:jspm-nodelibs-stream@0.2.1",
      "constants": "npm:jspm-nodelibs-constants@0.2.1",
      "assert": "npm:jspm-nodelibs-assert@0.2.1",
      "string_decoder": "npm:jspm-nodelibs-string_decoder@0.2.2",
      "events": "npm:jspm-nodelibs-events@0.2.2",
      "readline": "npm:jspm-nodelibs-readline@0.2.1",
      "child_process": "npm:jspm-nodelibs-child_process@0.2.1",
      "mocha": "npm:mocha@5.2.0",
      "tty": "npm:jspm-nodelibs-tty@0.2.1",
      "chai": "npm:chai@4.2.0",
      "sinon": "npm:sinon@7.5.0"
    },
    "packages": {
      "npm:jspm-nodelibs-os@0.2.2": {
        "map": {
          "os-browserify": "npm:os-browserify@0.3.0"
        }
      },
      "npm:jspm-nodelibs-crypto@0.2.1": {
        "map": {
          "crypto-browserify": "npm:crypto-browserify@3.12.0"
        }
      },
      "npm:crypto-browserify@3.12.0": {
        "map": {
          "browserify-cipher": "npm:browserify-cipher@1.0.1",
          "inherits": "npm:inherits@2.0.4",
          "create-ecdh": "npm:create-ecdh@4.0.3",
          "randomfill": "npm:randomfill@1.0.4",
          "browserify-sign": "npm:browserify-sign@4.0.4",
          "create-hmac": "npm:create-hmac@1.1.7",
          "public-encrypt": "npm:public-encrypt@4.0.3",
          "pbkdf2": "npm:pbkdf2@3.0.17",
          "diffie-hellman": "npm:diffie-hellman@5.0.3",
          "create-hash": "npm:create-hash@1.2.0",
          "randombytes": "npm:randombytes@2.1.0"
        }
      },
      "npm:browserify-sign@4.0.4": {
        "map": {
          "create-hmac": "npm:create-hmac@1.1.7",
          "inherits": "npm:inherits@2.0.4",
          "browserify-rsa": "npm:browserify-rsa@4.0.1",
          "bn.js": "npm:bn.js@4.11.8",
          "parse-asn1": "npm:parse-asn1@5.1.5",
          "elliptic": "npm:elliptic@6.5.2",
          "create-hash": "npm:create-hash@1.2.0"
        }
      },
      "npm:create-hmac@1.1.7": {
        "map": {
          "inherits": "npm:inherits@2.0.4",
          "safe-buffer": "npm:safe-buffer@5.1.2",
          "ripemd160": "npm:ripemd160@2.0.2",
          "cipher-base": "npm:cipher-base@1.0.4",
          "create-hash": "npm:create-hash@1.2.0",
          "sha.js": "npm:sha.js@2.4.11"
        }
      },
      "npm:browserify-cipher@1.0.1": {
        "map": {
          "browserify-des": "npm:browserify-des@1.0.2",
          "evp_bytestokey": "npm:evp_bytestokey@1.0.3",
          "browserify-aes": "npm:browserify-aes@1.2.0"
        }
      },
      "npm:pbkdf2@3.0.17": {
        "map": {
          "create-hmac": "npm:create-hmac@1.1.7",
          "safe-buffer": "npm:safe-buffer@5.1.2",
          "ripemd160": "npm:ripemd160@2.0.2",
          "create-hash": "npm:create-hash@1.2.0",
          "sha.js": "npm:sha.js@2.4.11"
        }
      },
      "npm:randomfill@1.0.4": {
        "map": {
          "safe-buffer": "npm:safe-buffer@5.1.2",
          "randombytes": "npm:randombytes@2.1.0"
        }
      },
      "npm:evp_bytestokey@1.0.3": {
        "map": {
          "safe-buffer": "npm:safe-buffer@5.1.2",
          "md5.js": "npm:md5.js@1.3.4"
        }
      },
      "npm:browserify-des@1.0.2": {
        "map": {
          "safe-buffer": "npm:safe-buffer@5.1.2",
          "inherits": "npm:inherits@2.0.4",
          "cipher-base": "npm:cipher-base@1.0.4",
          "des.js": "npm:des.js@1.0.1"
        }
      },
      "npm:create-ecdh@4.0.3": {
        "map": {
          "bn.js": "npm:bn.js@4.11.8",
          "elliptic": "npm:elliptic@6.5.2"
        }
      },
      "npm:diffie-hellman@5.0.3": {
        "map": {
          "bn.js": "npm:bn.js@4.11.8",
          "miller-rabin": "npm:miller-rabin@4.0.1",
          "randombytes": "npm:randombytes@2.1.0"
        }
      },
      "npm:browserify-rsa@4.0.1": {
        "map": {
          "bn.js": "npm:bn.js@4.11.8",
          "randombytes": "npm:randombytes@2.1.0"
        }
      },
      "npm:ripemd160@2.0.2": {
        "map": {
          "inherits": "npm:inherits@2.0.4",
          "hash-base": "npm:hash-base@3.0.4"
        }
      },
      "npm:cipher-base@1.0.4": {
        "map": {
          "inherits": "npm:inherits@2.0.4",
          "safe-buffer": "npm:safe-buffer@5.1.2"
        }
      },
      "npm:miller-rabin@4.0.1": {
        "map": {
          "bn.js": "npm:bn.js@4.11.8",
          "brorand": "npm:brorand@1.1.0"
        }
      },
      "npm:md5.js@1.3.4": {
        "map": {
          "inherits": "npm:inherits@2.0.4",
          "hash-base": "npm:hash-base@3.0.4"
        }
      },
      "npm:jspm-nodelibs-stream@0.2.1": {
        "map": {
          "stream-browserify": "npm:stream-browserify@2.0.2"
        }
      },
      "npm:create-hash@1.2.0": {
        "map": {
          "cipher-base": "npm:cipher-base@1.0.4",
          "inherits": "npm:inherits@2.0.4",
          "md5.js": "npm:md5.js@1.3.4",
          "ripemd160": "npm:ripemd160@2.0.2",
          "sha.js": "npm:sha.js@2.4.11"
        }
      },
      "npm:hash-base@3.0.4": {
        "map": {
          "inherits": "npm:inherits@2.0.4",
          "safe-buffer": "npm:safe-buffer@5.1.2"
        }
      },
      "npm:asn1.js@4.10.1": {
        "map": {
          "bn.js": "npm:bn.js@4.11.8",
          "inherits": "npm:inherits@2.0.4",
          "minimalistic-assert": "npm:minimalistic-assert@1.0.1"
        }
      },
      "npm:hmac-drbg@1.0.1": {
        "map": {
          "hash.js": "npm:hash.js@1.1.7",
          "minimalistic-crypto-utils": "npm:minimalistic-crypto-utils@1.0.1",
          "minimalistic-assert": "npm:minimalistic-assert@1.0.1"
        }
      },
      "npm:browserify-aes@1.2.0": {
        "map": {
          "cipher-base": "npm:cipher-base@1.0.4",
          "create-hash": "npm:create-hash@1.2.0",
          "evp_bytestokey": "npm:evp_bytestokey@1.0.3",
          "inherits": "npm:inherits@2.0.4",
          "safe-buffer": "npm:safe-buffer@5.1.2",
          "buffer-xor": "npm:buffer-xor@1.0.3"
        }
      },
      "npm:jspm-nodelibs-string_decoder@0.2.2": {
        "map": {
          "string_decoder": "npm:string_decoder@0.10.31"
        }
      },
      "npm:string_decoder@1.1.1": {
        "map": {
          "safe-buffer": "npm:safe-buffer@5.1.2"
        }
      },
      "npm:sha.js@2.4.11": {
        "map": {
          "inherits": "npm:inherits@2.0.4",
          "safe-buffer": "npm:safe-buffer@5.1.2"
        }
      },
      "npm:mocha@5.2.0": {
        "map": {
          "browser-stdout": "npm:browser-stdout@1.3.1",
          "commander": "npm:commander@2.15.1",
          "growl": "npm:growl@1.10.5",
          "he": "npm:he@1.1.1",
          "diff": "npm:diff@3.5.0",
          "escape-string-regexp": "npm:escape-string-regexp@1.0.5",
          "glob": "npm:glob@7.1.2",
          "mkdirp": "npm:mkdirp@0.5.1",
          "minimatch": "npm:minimatch@3.0.4",
          "supports-color": "npm:supports-color@5.4.0",
          "debug": "npm:debug@3.1.0",
          "node-growl": "npm:growl@1.10.5",
          "node-glob": "npm:glob@7.1.2",
          "node-supports-color": "npm:supports-color@5.4.0"
        }
      },
      "npm:glob@7.1.2": {
        "map": {
          "minimatch": "npm:minimatch@3.0.4",
          "inherits": "npm:inherits@2.0.4",
          "inflight": "npm:inflight@1.0.6",
          "fs.realpath": "npm:fs.realpath@1.0.0",
          "once": "npm:once@1.4.0",
          "path-is-absolute": "npm:path-is-absolute@1.0.1"
        }
      },
      "npm:inflight@1.0.6": {
        "map": {
          "once": "npm:once@1.4.0",
          "wrappy": "npm:wrappy@1.0.2"
        }
      },
      "npm:once@1.4.0": {
        "map": {
          "wrappy": "npm:wrappy@1.0.2"
        }
      },
      "npm:debug@3.1.0": {
        "map": {
          "ms": "npm:ms@2.0.0"
        }
      },
      "npm:mkdirp@0.5.1": {
        "map": {
          "minimist": "npm:minimist@0.0.8"
        }
      },
      "npm:minimatch@3.0.4": {
        "map": {
          "brace-expansion": "npm:brace-expansion@1.1.11"
        }
      },
      "npm:brace-expansion@1.1.11": {
        "map": {
          "concat-map": "npm:concat-map@0.0.1",
          "balanced-match": "npm:balanced-match@1.0.0"
        }
      },
      "npm:supports-color@5.4.0": {
        "map": {
          "has-flag": "npm:has-flag@3.0.0"
        }
      },
      "npm:chai@4.2.0": {
        "map": {
          "assertion-error": "npm:assertion-error@1.1.0",
          "check-error": "npm:check-error@1.0.2",
          "pathval": "npm:pathval@1.1.0",
          "get-func-name": "npm:get-func-name@2.0.0",
          "deep-eql": "npm:deep-eql@3.0.1",
          "type-detect": "npm:type-detect@4.0.8"
        }
      },
      "npm:deep-eql@3.0.1": {
        "map": {
          "type-detect": "npm:type-detect@4.0.8"
        }
      },
      "npm:sinon@7.5.0": {
        "map": {
          "supports-color": "npm:supports-color@5.5.0",
          "diff": "npm:diff@3.5.0",
          "nise": "npm:nise@1.5.3",
          "lolex": "npm:lolex@4.2.0",
          "@sinonjs/commons": "npm:@sinonjs/commons@1.7.0",
          "@sinonjs/formatio": "npm:@sinonjs/formatio@3.2.2",
          "@sinonjs/samsam": "npm:@sinonjs/samsam@3.3.3"
        }
      },
      "npm:supports-color@5.5.0": {
        "map": {
          "has-flag": "npm:has-flag@3.0.0"
        }
      },
      "npm:nise@1.5.3": {
        "map": {
          "lolex": "npm:lolex@5.1.2",
          "just-extend": "npm:just-extend@4.0.2",
          "path-to-regexp": "npm:path-to-regexp@1.8.0",
          "@sinonjs/text-encoding": "npm:@sinonjs/text-encoding@0.7.1",
          "@sinonjs/formatio": "npm:@sinonjs/formatio@3.2.2",
          "node-@sinonjs/text-encoding": "npm:@sinonjs/text-encoding@0.7.1"
        }
      },
      "npm:@sinonjs/formatio@3.2.2": {
        "map": {
          "@sinonjs/commons": "npm:@sinonjs/commons@1.7.0",
          "@sinonjs/samsam": "npm:@sinonjs/samsam@3.3.3"
        }
      },
      "npm:@sinonjs/samsam@3.3.3": {
        "map": {
          "@sinonjs/commons": "npm:@sinonjs/commons@1.7.0",
          "array-from": "npm:array-from@2.1.1",
          "lodash": "npm:lodash@4.17.15"
        }
      },
      "npm:@sinonjs/commons@1.7.0": {
        "map": {
          "type-detect": "npm:type-detect@4.0.8"
        }
      },
      "npm:lolex@5.1.2": {
        "map": {
          "@sinonjs/commons": "npm:@sinonjs/commons@1.7.0"
        }
      },
      "npm:stream-browserify@2.0.2": {
        "map": {
          "readable-stream": "npm:readable-stream@2.3.7",
          "inherits": "npm:inherits@2.0.4"
        }
      },
      "npm:readable-stream@2.3.7": {
        "map": {
          "string_decoder": "npm:string_decoder@1.1.1",
          "process-nextick-args": "npm:process-nextick-args@2.0.1",
          "core-util-is": "npm:core-util-is@1.0.2",
          "util-deprecate": "npm:util-deprecate@1.0.2",
          "inherits": "npm:inherits@2.0.4",
          "isarray": "npm:isarray@1.0.0",
          "safe-buffer": "npm:safe-buffer@5.1.2"
        }
      },
      "npm:elliptic@6.5.2": {
        "map": {
          "inherits": "npm:inherits@2.0.4",
          "bn.js": "npm:bn.js@4.11.8",
          "hmac-drbg": "npm:hmac-drbg@1.0.1",
          "hash.js": "npm:hash.js@1.1.7",
          "minimalistic-assert": "npm:minimalistic-assert@1.0.1",
          "brorand": "npm:brorand@1.1.0",
          "minimalistic-crypto-utils": "npm:minimalistic-crypto-utils@1.0.1"
        }
      },
      "npm:des.js@1.0.1": {
        "map": {
          "inherits": "npm:inherits@2.0.4",
          "minimalistic-assert": "npm:minimalistic-assert@1.0.1"
        }
      },
      "npm:path-to-regexp@1.8.0": {
        "map": {
          "isarray": "npm:isarray@0.0.1"
        }
      },
      "npm:public-encrypt@4.0.3": {
        "map": {
          "parse-asn1": "npm:parse-asn1@5.1.5",
          "create-hash": "npm:create-hash@1.2.0",
          "browserify-rsa": "npm:browserify-rsa@4.0.1",
          "bn.js": "npm:bn.js@4.11.8",
          "randombytes": "npm:randombytes@2.1.0",
          "safe-buffer": "npm:safe-buffer@5.1.2"
        }
      },
      "npm:parse-asn1@5.1.5": {
        "map": {
          "asn1.js": "npm:asn1.js@4.10.1",
          "browserify-aes": "npm:browserify-aes@1.2.0",
          "create-hash": "npm:create-hash@1.2.0",
          "evp_bytestokey": "npm:evp_bytestokey@1.0.3",
          "pbkdf2": "npm:pbkdf2@3.0.17",
          "safe-buffer": "npm:safe-buffer@5.1.2"
        }
      },
      "npm:randombytes@2.1.0": {
        "map": {
          "safe-buffer": "npm:safe-buffer@5.1.2"
        }
      },
      "npm:hash.js@1.1.7": {
        "map": {
          "inherits": "npm:inherits@2.0.4",
          "minimalistic-assert": "npm:minimalistic-assert@1.0.1"
        }
      },
      "npm:typescript@2.9.2": {
        "map": {
          "source-map-support": "npm:source-map-support@0.5.16"
        }
      }
    }
  },
  transpiler: "plugin-typescript",
  packages: {
    "app": {
      "main": "app.ts",
      "format": "esm",
      "defaultExtension": "ts",
      "meta": {
        "*.ts": {
          "loader": "plugin-typescript"
        }
      }
    },
    "test": {
      "defaultExtension": "ts",
      "meta": {
        "*.ts": {
          "loader": "plugin-typescript"
        }
      }
    }
  }
});

SystemJS.config({
  packageConfigPaths: [
    "github:*/*.json",
    "npm:@*/*.json",
    "npm:*.json"
  ],
  map: {
    "aurelia-binding": "npm:aurelia-binding@2.5.2",
    "aurelia-bootstrapper": "npm:aurelia-bootstrapper@2.3.3",
    "aurelia-dependency-injection": "npm:aurelia-dependency-injection@1.5.2",
    "aurelia-event-aggregator": "npm:aurelia-event-aggregator@1.0.3",
    "aurelia-framework": "npm:aurelia-framework@1.3.1",
    "aurelia-history": "npm:aurelia-history@1.2.1",
    "aurelia-history-browser": "npm:aurelia-history-browser@1.4.0",
    "aurelia-loader": "npm:aurelia-loader@1.0.2",
    "aurelia-loader-default": "npm:aurelia-loader-default@1.2.1",
    "aurelia-logging": "npm:aurelia-logging@1.5.0",
    "aurelia-logging-console": "npm:aurelia-logging-console@1.1.1",
    "aurelia-metadata": "npm:aurelia-metadata@1.0.4",
    "aurelia-pal": "npm:aurelia-pal@1.8.2",
    "aurelia-pal-browser": "npm:aurelia-pal-browser@1.8.1",
    "aurelia-path": "npm:aurelia-path@1.1.5",
    "aurelia-polyfills": "npm:aurelia-polyfills@1.3.4",
    "aurelia-route-recognizer": "npm:aurelia-route-recognizer@1.3.2",
    "aurelia-router": "npm:aurelia-router@1.7.1",
    "aurelia-task-queue": "npm:aurelia-task-queue@1.3.1",
    "aurelia-templating": "npm:aurelia-templating@1.10.2",
    "aurelia-templating-binding": "npm:aurelia-templating-binding@1.5.3",
    "aurelia-templating-resources": "npm:aurelia-templating-resources@1.12.0",
    "aurelia-templating-router": "npm:aurelia-templating-router@1.4.0",
    "buffer": "npm:jspm-nodelibs-buffer@0.2.3",
    "fs": "npm:jspm-nodelibs-fs@0.2.1",
    "module": "npm:jspm-nodelibs-module@0.2.1",
    "path": "npm:jspm-nodelibs-path@0.2.3",
    "process": "npm:jspm-nodelibs-process@0.2.1",
    "source-map-support": "npm:source-map-support@0.5.16",
    "text": "github:systemjs/plugin-text@0.0.7"
  },
  packages: {
    "npm:jspm-nodelibs-buffer@0.2.3": {
      "map": {
        "buffer": "npm:buffer@5.4.3"
      }
    },
    "npm:aurelia-history-browser@1.4.0": {
      "map": {
        "aurelia-history": "npm:aurelia-history@1.2.1",
        "aurelia-pal": "npm:aurelia-pal@1.8.2"
      }
    },
    "npm:aurelia-logging-console@1.1.1": {
      "map": {
        "aurelia-logging": "npm:aurelia-logging@1.5.2"
      }
    },
    "npm:aurelia-event-aggregator@1.0.3": {
      "map": {
        "aurelia-logging": "npm:aurelia-logging@1.5.2"
      }
    },
    "npm:aurelia-loader-default@1.2.1": {
      "map": {
        "aurelia-loader": "npm:aurelia-loader@1.0.2",
        "aurelia-metadata": "npm:aurelia-metadata@1.0.4",
        "aurelia-pal": "npm:aurelia-pal@1.8.2"
      }
    },
    "npm:aurelia-loader@1.0.2": {
      "map": {
        "aurelia-metadata": "npm:aurelia-metadata@1.0.4",
        "aurelia-path": "npm:aurelia-path@1.1.5"
      }
    },
    "npm:aurelia-route-recognizer@1.3.2": {
      "map": {
        "aurelia-path": "npm:aurelia-path@1.1.5"
      }
    },
    "npm:aurelia-templating-binding@1.5.3": {
      "map": {
        "aurelia-binding": "npm:aurelia-binding@2.5.2",
        "aurelia-logging": "npm:aurelia-logging@1.5.2",
        "aurelia-templating": "npm:aurelia-templating@1.10.2"
      }
    },
    "npm:aurelia-bootstrapper@2.3.3": {
      "map": {
        "aurelia-event-aggregator": "npm:aurelia-event-aggregator@1.0.3",
        "aurelia-framework": "npm:aurelia-framework@1.3.1",
        "aurelia-history": "npm:aurelia-history@1.2.1",
        "aurelia-history-browser": "npm:aurelia-history-browser@1.4.0",
        "aurelia-loader-default": "npm:aurelia-loader-default@1.2.1",
        "aurelia-logging-console": "npm:aurelia-logging-console@1.1.1",
        "aurelia-pal": "npm:aurelia-pal@1.8.2",
        "aurelia-pal-browser": "npm:aurelia-pal-browser@1.8.1",
        "aurelia-polyfills": "npm:aurelia-polyfills@1.3.4",
        "aurelia-router": "npm:aurelia-router@1.7.1",
        "aurelia-templating": "npm:aurelia-templating@1.10.2",
        "aurelia-templating-binding": "npm:aurelia-templating-binding@1.5.3",
        "aurelia-templating-resources": "npm:aurelia-templating-resources@1.12.0",
        "aurelia-templating-router": "npm:aurelia-templating-router@1.4.0"
      }
    },
    "npm:aurelia-polyfills@1.3.4": {
      "map": {
        "aurelia-pal": "npm:aurelia-pal@1.8.2"
      }
    },
    "npm:aurelia-templating@1.10.2": {
      "map": {
        "aurelia-binding": "npm:aurelia-binding@2.5.2",
        "aurelia-dependency-injection": "npm:aurelia-dependency-injection@1.5.2",
        "aurelia-loader": "npm:aurelia-loader@1.0.2",
        "aurelia-logging": "npm:aurelia-logging@1.5.2",
        "aurelia-metadata": "npm:aurelia-metadata@1.0.4",
        "aurelia-pal": "npm:aurelia-pal@1.8.2",
        "aurelia-path": "npm:aurelia-path@1.1.5",
        "aurelia-task-queue": "npm:aurelia-task-queue@1.3.1"
      }
    },
    "npm:aurelia-binding@2.5.2": {
      "map": {
        "aurelia-logging": "npm:aurelia-logging@1.5.2",
        "aurelia-metadata": "npm:aurelia-metadata@1.0.4",
        "aurelia-pal": "npm:aurelia-pal@1.8.2",
        "aurelia-task-queue": "npm:aurelia-task-queue@1.3.1"
      }
    },
    "npm:source-map-support@0.5.16": {
      "map": {
        "buffer-from": "npm:buffer-from@1.1.1",
        "source-map": "npm:source-map@0.6.1"
      }
    },
    "npm:buffer@5.4.3": {
      "map": {
        "base64-js": "npm:base64-js@1.3.1",
        "ieee754": "npm:ieee754@1.1.13"
      }
    }
  }
});
