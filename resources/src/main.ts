import {Aurelia} from 'aurelia-framework';


export function configure(aurelia: Aurelia): void {
    aurelia.use.standardConfiguration();

    if (!SystemJS.production) {
        // configuration for development mode
        aurelia.use.developmentLogging();
    }

    if (SystemJS.mockApi) {
        console.log("Using mock api feature");
    }

    aurelia.start().then(() => aurelia.setRoot());
}

