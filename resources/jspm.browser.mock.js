SystemJS.config({
  baseURL: ".",
  mockApi: true,
  paths: {
    "github:": "jspm_packages/github/",
    "npm:": "jspm_packages/npm/",
    "app/": "src/"
  }
});
