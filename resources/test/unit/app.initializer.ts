import { App } from '../../src/app';

export class AppInitializer {

    build() {
        return new App();
    }    
}
