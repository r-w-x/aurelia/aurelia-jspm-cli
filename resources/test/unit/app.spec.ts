import { App } from '../../src/app';

import {AppInitializer} from "./app.initializer";

import sinon from "sinon";
import chai from 'chai';
import 'mocha';

const expect = chai.expect;

describe("App Test Suite", () => {
    let sandbox = sinon.createSandbox();
    
    afterEach(() => {
        sandbox.restore();
    });

    it('should setup default message', () => {
        var app: App = new App();
        expect(app.message).to.be.equal("It Works ");
    });
    
    it('should setup default message using another test class', () => {
        var app: App = new AppInitializer().build();
        expect(app.message).to.be.equal("It Works ");
    });
    
    it('should add exclaim to message message', () => {
        var app: App = new App();
        app.exclaim();
        expect(app.message).to.be.equal("It Works !!");
    });
    
    it('should have mocked test', () => {
        let spy = sandbox.spy();
        spy("fake argument");
        expect(spy.calledOnce).to.be.true;
        expect(spy.lastCall.args[0]).to.equal("fake argument");
    })

    
    it('should fail having too much exclaim', () => {
        var app: App = new App();
        app.exclaim();
        app.exclaim();
        expect(app.message).to.be.equal("It Works ");
    });
})