"use strict";

var fs = require('fs');
var path = require('path');


module.exports = function () {
    console.log("Setting up site structure...");
    var resourcePath = path.dirname(require.resolve('../../resources/index.html'));
    var destPath = process.cwd();
    copyResources(resourcePath, destPath);
    for (var iResource = 0; iResource < copiedResource.length - 1; iResource++) {
        console.log("├── " + path.relative(resourcePath, copiedResource[iResource]));
    }
    console.log("└── " + path.relative(resourcePath, copiedResource[copiedResource.length - 1]));
    console.log(" ");
//    }).then(function () {
    console.log("Adding NPM scripts...");
    var projectJson = JSON.parse(fs.readFileSync(process.cwd() + '/package.json', 'utf8'));
    if (!projectJson.scripts) {
        projectJson.scripts = {};
    }

    console.log("├── Post-install script");
    projectJson.scripts["postinstall"] = "jspm install";
    console.log("├── Script to bundle vendor");
    projectJson.scripts["bundle:vendor"] = "./node_modules/aurelia-jspm-cli/bin/auj.js bundle --vendor";
    console.log("├── Script to bundle application");
    projectJson.scripts["bundle:app"] = "./node_modules/aurelia-jspm-cli/bin/auj.js bundle --app";
    console.log("├── Script to bundle vendor and application");
    projectJson.scripts["bundle"] = "./node_modules/aurelia-jspm-cli/bin/auj.js bundle --app --vendor";
    console.log("├── Script to launch test");
    projectJson.scripts["test"] = "./node_modules/karma-cli/bin/karma start";
    console.log("├── Script to start development server with mocked api");
    projectJson.scripts["start:mockapi"] = "./node_modules/aurelia-jspm-cli/bin/auj.js start --mockapi";
    console.log("└── Script to start development server");
    projectJson.scripts["start"] = "./node_modules/aurelia-jspm-cli/bin/auj.js start";

    fs.writeFileSync(process.cwd() + '/package.json', JSON.stringify(projectJson, null, '  '));
    console.log(" ");
};

var copiedResource = [];
function copyResources(resourcePath, destPath) {
    var files = fs.readdirSync(resourcePath);
    for (var file in files) {
        var childPath = path.join(resourcePath, files[file]);
        var childDestPath = path.join(destPath, files[file]);
        if( ""+files[file] === "gitignore" ) {
            childDestPath = path.join(destPath, ".gitignore");
        }
        if (fs.statSync(childPath).isDirectory()) {
            fs.mkdirSync(childDestPath);
            copyResources(childPath, childDestPath);
        } else {
            if( ""+files[file] === "gitignore" ) {
                copiedResource.push(path.join(resourcePath, ".gitignore"));
            }else {
                copiedResource.push(childPath);
            }
            var contents = fs.readFileSync(childPath).toString();
            fs.writeFileSync(childDestPath, contents);
        }
    }
}